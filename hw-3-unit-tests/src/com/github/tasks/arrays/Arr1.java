package com.github.tasks.arrays;
import java.util.Scanner;

public class Arr1 {

    public static void main(String[] args){
        int[] a;
        int n;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter number of array: ");
        n = in.nextInt();
        a = new int [n];
        for (int i = 0; i < n; i++){
            System.out.print("Enter a["+i+"] = ");
            a[i] = in.nextInt();
        }
        System.out.println("min: " + minEl(n, a));
    }

    public static int minEl(int n, int[] a) {
        int min = a[0];
        for(int i = 0; i < n; i ++){
            if(min>a[i])
                min = a[i];
        }
        return min;
    }
}
