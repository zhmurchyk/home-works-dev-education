package com.github.tasks.arrays;
import java.util.Scanner;

public class Arr6 {

    public static void main(String[] args){
        int[] a, b;
        int n;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter number of array: ");
        n = in.nextInt();
        a = new int [n];
        for (int i = 0; i < n; i++){
            System.out.print("Enter a["+i+"] = ");
            a[i] = in.nextInt();
        }
        b = rev(n, a);
        for (int i = 0; i < n; i++)
            System.out.println("b["+i+"] = " + b[i]);
    }

    public static int [] rev(int n, int [] a) {
        for (int i = 0; i < n / 2; i++) {
            int temp = a[i];
            a[i] = a[n - 1 - i];
            a[n - 1 - i] = temp;
        }
        return a;
    }
}
