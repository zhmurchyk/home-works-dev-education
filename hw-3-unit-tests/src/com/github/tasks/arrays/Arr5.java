package com.github.tasks.arrays;
import java.util.Scanner;

public class Arr5 {

    public static void main(String[] args){
        int[] a;
        int n;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter number of array: ");
        n = in.nextInt();
        a = new int [n];
        for (int i = 0; i < n; i++){
            System.out.print("Enter a["+i+"] = ");
            a[i] = in.nextInt();
        }

        System.out.println("sum: " + sum(n, a));
    }

    public static int sum(int n, int[] a) {
        int sum = 0;
        for(int i = 0; i < n; i ++){
            if (i % 2 != 0)
                sum += a[i];
        }
        return sum;
    }
}
