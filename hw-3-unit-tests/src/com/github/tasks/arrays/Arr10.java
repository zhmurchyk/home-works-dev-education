package com.github.tasks.arrays;
import java.util.ArrayList;

public class Arr10 {
    public static void main(String[] args) {
        new Arr10();
    }

    private final ArrayList<String> wordList = new ArrayList<>();

    public Arr10() {
        wordList.add("Quick");
        wordList.add("Merge");
        wordList.add("Shell");
        wordList.add("Heap");
        alphaSort();
    }

    public void alphaSort() {
        String[] alphaList = new String[wordList.size()];
        int count = 0;
        while (count < wordList.size()) {
            alphaList[count] = wordList.get(count);
            count++;
        }
        int shortestStringIndex;
        for (int j = 0; j < alphaList.length - 1; j++) {
            shortestStringIndex = j;
            for (int i = j + 1; i < alphaList.length; i++) {
                if (alphaList[i].trim().compareTo(alphaList[shortestStringIndex].trim()) < 0) {
                    shortestStringIndex = i;
                }
            }
            if (shortestStringIndex != j) {
                String temp = alphaList[j];
                alphaList[j] = alphaList[shortestStringIndex];
                alphaList[shortestStringIndex] = temp;
            }
        }
        count = 0;
        while (count < alphaList.length) {
            System.out.println(alphaList[count++]);
        }
    }
}

