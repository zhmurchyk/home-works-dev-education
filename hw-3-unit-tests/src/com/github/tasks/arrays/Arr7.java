package com.github.tasks.arrays;
import java.util.Scanner;

public class Arr7 {

    public static void main(String[] args){
        int[] a;
        int n;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter number of array: ");
        n = in.nextInt();
        a = new int [n];
        for (int i = 0; i < n; i++){
            System.out.print("Enter a["+i+"] = ");
            a[i] = in.nextInt();
        }
        System.out.println("k: " + countOdd(n, a));
    }

    public static int countOdd(int n, int [] a) {
        int k = 0;
        for (int i = 0; i < n; i++) {
            if (a[i] % 2 != 0) {
                k++;
            }
        }
        return k;
    }
}
