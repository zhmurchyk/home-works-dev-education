package com.github.tasks.arrays;
import java.util.Scanner;

public class Arr8 {

    public static void main(String[] args){
        int[] a, b;
        int n;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter even number of array: ");
        n = in.nextInt();
        a = new int [n];
        for (int i = 0; i < n; i++){
            System.out.print("Enter a["+i+"] = ");
            a[i] = in.nextInt();
        }
        b = replace(n, a);
        for (int i = 0; i < n; i++)
            System.out.println("b["+i+"] = " + b[i]);
    }

    public static int [] replace(int n, int [] a) {
        int count1 = 0;
        int count2 = n / 2;

        while (count1 < n / 2)
        {
            int temp = a[count1];
            a[count1] = a[count2];
            a[count2] = temp;

            count1++; count2++;
        }
        return a;
    }
}

