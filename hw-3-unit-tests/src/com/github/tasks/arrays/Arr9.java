package com.github.tasks.arrays;
import java.util.Scanner;

public class Arr9 {

    public static void main(String[] args){
        int[] a, a1, a2, a3;
        int n;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter number of array: ");
        n = in.nextInt();
        a = new int [n];
        for (int i = 0; i < n; i++){
            System.out.print("Enter a["+i+"] = ");
            a[i] = in.nextInt();
        }
        a1 = bubbleSort(n, a);
        for (int i = 0; i < n; i++)
            System.out.println("a1["+i+"] = " + a1[i]);
        a2 = selectSort(n, a);
        for (int i = 0; i < n; i++)
            System.out.println("a2["+i+"] = " + a2[i]);
        a3 = insertSort(n, a);
        for (int i = 0; i < n; i++)
            System.out.println("a3["+i+"] = " + a3[i]);
    }

    public static int [] bubbleSort(int n, int [] a) {
        for (int i = 0; i < n - 1; i++) {
            for (int j = n - 1; j > i; j--) {
                if (a[j - 1] > a[j]) {
                    int tmp = a[j - 1];
                    a[j - 1] = a[j];
                    a[j] = tmp;
                }
            }
        }
        return a;
    }
    public static int [] selectSort(int n, int [] a) {
        for (int i = 0; i < n; i++) {
            int pos = i;
            int min = a[i];
            for (int j = i + 1; j < n; j++) {
                if (a[j] < min) {
                    pos = j;
                    min = a[j];
                }
            }
            a[pos] = a[i];
            a[i] = min;
        }
        return a;
    }

    public static int [] insertSort(int n, int [] a) {
        for (int i = 1; i < n; i++) {
            int key = a[i];
            int j = i - 1;
            while (j >= 0 && a[j]> key) {
                a[j + 1]= a[j];
                j = j - 1;
            }
            a[j + 1]= key;
        }
        return a;
    }
}

