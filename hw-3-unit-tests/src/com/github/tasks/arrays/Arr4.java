package com.github.tasks.arrays;
import java.util.Scanner;

public class Arr4 {

    public static void main(String[] args){
        int[] a;
        int n;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter number of array: ");
        n = in.nextInt();
        a = new int [n];
        for (int i = 0; i < n; i++){
            System.out.print("Enter a["+i+"] = ");
            a[i] = in.nextInt();
        }
        System.out.println("max: a[" + maxElIndex(n, a) + "]");
    }

    public static int maxElIndex(int n, int [] a) {
        int max = a[0], k = 0;
        for (int i = 0; i < n; i ++){
            if(max<a[i]) {
                max = a[i];
                k = i;
            }
        }
        return k;
    }
}
