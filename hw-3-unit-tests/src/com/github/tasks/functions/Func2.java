package com.github.tasks.functions;
import java.util.Scanner;

public class Func2 {
    public static final String[] twenty = { "ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять", "одинадцать", "двенадцадь", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать" };
    public static final String[] tens = { "сто","десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто" };
    public static final String[] hundreds = { "тысяча","сто", "двести", "триста", "четириста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот" };

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.print("Число от 0 до 999: ");
            if (!scanner.hasNextInt())
                break;
            int number = scanner.nextInt();
            if (number < 0 || number > 999) {
                System.out.println("Попробуй ещё раз...");
                continue;
            }
           toStr(number);
        }
        scanner.close();

    }
     public static void toStr(int number){
            if ( number < 20 )
                System.out.print(twenty[number]);
            else if ( number < 100 ) {
                int high = number / 10;
                int low = number % 10;
                String text = tens[high];
                if ( low != 0 )
                    text = text + " " + twenty[low];
                System.out.print(text);
            }
            else if ( number < 1000 ) {
                int high2 = number / 100;
                int high = (number - number % 10) / 10 % 10;
                int low = number % 10;
                String text = hundreds[high2];
                if ( high > 1 )
                    text = text + " " + tens[high];
                if (high == 1)
                    low += 10;
                if ( low != 0 )
                    text = text + " " + twenty[low];
                System.out.print(text);
            }
            else
                System.out.print(tens[0]);
        }
    }
