package com.github.tasks.functions;
import java.util.Scanner;

public class Func1 {

    public static void main(String[] args) {
        String[] a = new String[]{"MON", "TUE", "WED", "THU", "FRA", "SAT", "SUN"};
        int n;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter number of day of week: ");
        n = in.nextInt();
        func(n, a);
    }

    public static void func(int n, String[] a) {
        if (n < 1 || n > 7)
            System.out.print("Wrong number of day of week!!!");
        else {
            String res = a [n-1];
            System.out.print(res);
        }
    }
}
