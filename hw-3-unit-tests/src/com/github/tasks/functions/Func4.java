package com.github.tasks.functions;
import java.util.Scanner;

public class Func4 {

    public static void main(String[] args) {
        double x1, x2, y1, y2;
        Scanner in = new Scanner(System.in);
        System.out.print("x1 ");
        x1 = in.nextInt();
        System.out.print("y1 ");
        y1 = in.nextInt();
        System.out.print("x2 ");
        x2 = in.nextInt();
        System.out.print("y2 ");
        y2 = in.nextInt();

        double res = dist( x1, y1, x2, y2);
        System.out.print(res);
    }

    public static double dist( double x1, double y1, double x2, double y2) {
        return Math.sqrt((y2 - y1) *  (y2 - y1) + (x2 - x1) *  (x2 - x1));
    }
}
