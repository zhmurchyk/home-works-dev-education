package com.github.tasks.conditionaloperators;
import java.util.Scanner;

public class CO3 {
    public static void main(String[] args) {
        Scanner num = new Scanner(System.in);
        int a, b, c;
        System.out.print("Enter num a: ");
        a = num.nextInt();
        System.out.print("Enter num b: ");
        b = num.nextInt();
        System.out.print("Enter num c: ");
        c = num.nextInt();
        System.out.print("Sum of positive numbers: " + sum(a, b, c));
    }

    public static int sum(int a, int b, int c) {
        int x = 0;
        if ( a > 0)
            x += a;
        if ( b > 0)
            x += b;
        if ( c > 0)
            x += c;
        return x;
    }
}