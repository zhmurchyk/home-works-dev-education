package com.github.tasks.conditionaloperators;
import java.util.Scanner;

public class CO5 {
    public static void main(String[] args) {
        Scanner num = new Scanner(System.in);
        int x;
        System.out.print("Enter rank from 0 to 100: ");
        x = num.nextInt();
        checkRank(x);
    }

    public static void checkRank(int x) {
        if ( x >= 0 && x < 20)
            System.out.print("Mark F");
        else if ( x >= 20 && x < 40)
            System.out.print("Mark E");
        else if ( x >= 40 && x < 60)
            System.out.print("Mark D");
        else if ( x >= 60 && x < 75)
            System.out.print("Mark C");
        else if ( x >= 75 && x < 90)
            System.out.print("Mark B");
        else if ( x >= 90 && x <= 100)
            System.out.print("Mark A");
        else
            System.out.print("Wrong rank");
    }
}

