package com.github.tasks.conditionaloperators;
import java.util.Scanner;

public class CO2 {
    public static void main(String[] args) {
        Scanner num = new Scanner(System.in);
        float x, y;
        System.out.print("Enter num x: ");
        x = num.nextFloat();
        System.out.print("Enter num y: ");
        y = num.nextFloat();
        System.out.println(checkQuarter(x, y));

    }

    public static String checkQuarter(float x, float y) {
        if ( x > 0 && y > 0)
            return "point in first quarter";
        else if ( x < 0 && y > 0)
            return "point in second quarter";
        else if ( x < 0 && y < 0)
            return "point in third quarter";
        else if ( x > 0 && y < 0)
            return "point in fourth quarter";
        else
            return "point on the coordinate axis";
    }
}
