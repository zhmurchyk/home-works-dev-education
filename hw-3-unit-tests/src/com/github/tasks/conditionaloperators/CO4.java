package com.github.tasks.conditionaloperators;
import java.util.Scanner;

public class CO4 {
    public static void main(String[] args) {
        Scanner num = new Scanner(System.in);
        int a, b, c;
        System.out.print("Enter num a: ");
        a = num.nextInt();
        System.out.print("Enter num b: ");
        b = num.nextInt();
        System.out.print("Enter num c: ");
        c = num.nextInt();
        System.out.print("max(a*b*c,a+b+c)+3 = " + maxIf(a, b, c));
    }

    public  static int maxIf(int a, int b, int c) {
        int x;
        if ( a * b * c > a + b + c) {
            x = a * b * c + 3;
            return x;
        }
        else {
            x = a + b + c + 3;
            return x;
        }
    }
}


