package com.github.tasks.conditionaloperators;

import java.util.Scanner;

public class CO1 {
    public static void main(String[] args) {
        Scanner num = new Scanner(System.in);
        System.out.print("Enter num a: ");
        int a = num.nextInt();
        System.out.print("Enter num b: ");
        float b = num.nextFloat();
        System.out.println(calc(a, b));

    }

    public static String calc(int a, float b) {
        String result;
        if (a % 2 == 0) {
            b = a * b;
            result = ("a * b = " + b);
        } else {
            b = a + b;
            result = ("a + b = " + b);
        }
        return result;
    }
}
