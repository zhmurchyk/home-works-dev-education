package com.github.tasks.loops;

public class Loop1 {
    public static void main(String[] args) {
        System.out.println("sum of even numbers: " + sum());
        System.out.println("number of even numbers: " + num());
    }

    public static int sum() {
        int k = 0;

        for (int i = 2; i < 100; i+=2) {
            k += i;
        }
        return k;
    }

    public static int num() {
        int k = 0;

        for (int i = 2; i < 100; i+=2) {
            k++;
        }
        return k;
    }

}