package com.github.tasks.loops;
import java.util.Scanner;

public class Loop3 {
    public static void main(String[] args) {
        Scanner num = new Scanner(System.in);
        System.out.print("Enter the number: ");
        int x = num.nextInt();
        System.out.println("root1 = " + root1(x));
        System.out.print("root2 = " + root2(x));
    }

    public static int root1(int x) {
        int k1;
        for (int i = 1; ; i++ )
        {
            int q = i * i;
            if (x == q) {
                k1 = i;
                break;
            }
            if (x < q) {
                k1 = i - 1;
                break;
            }
        }
        return k1;
    }

    public static int root2(int x) {
        int k2;
        int min = 1;
        int max = x;
        int prev = 0;
        for (;;)
        {
            int mid = (min + max) / 2;
            if (prev == mid) {
                k2 = mid;
                break;
            }
            int q = mid * mid;
            if (x == q) {
                k2 = mid;
                break;
            }
            if (x < q)
                max = mid;
            else
                min = mid;
            prev = mid;
        }
        return k2;
    }
}

