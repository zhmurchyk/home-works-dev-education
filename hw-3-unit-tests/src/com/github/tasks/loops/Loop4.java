package com.github.tasks.loops;
import java.util.Scanner;

public class Loop4 {
    public static void main(String[] args) {
        Scanner num = new Scanner(System.in);
        System.out.print("Enter num n: ");
        int n = num.nextInt();
        System.out.print("n! = " + fact(n));
    }

    public static int fact(int n) {
        int x = 1;
        for (int i = 1; i <= n; i++) {
            x = x * i;
        }
        return x;
    }
}
