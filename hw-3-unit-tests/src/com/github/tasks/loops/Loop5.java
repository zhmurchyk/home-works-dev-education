package com.github.tasks.loops;
import java.util.Scanner;

public class Loop5 {
    public static void main(String[] args) {
        Scanner num = new Scanner(System.in);
        int n;
        System.out.print("Enter num n: ");
        n = num.nextInt();
        System.out.println("Сумма цифр в числе " + n + " = " + sum(n));
    }

    public static int sum(int n) {
        int s = 0;
        int x = n;
        while (x != 0) {
            s += x % 10;
            x /= 10;
        }
        return Math.abs(s);
    }
}
