package com.github.tasks.loops;
import java.util.Scanner;

public class Loop2 {
    public static void main(String[] args) {
        Scanner scan= new Scanner(System.in);
        System.out.print("Enter a number to check > 1 : ");
        int num = scan.nextInt();
        if(isPrime(num)) {
            System.out.println(num + " - prime number");
        } else {
            System.out.println(num + " - composite number");
        }
    }
    public static boolean isPrime(int num) {
        int temp;
        boolean k=true;
        for (int i=2; i<=num/2; i++) {
            temp = num % i;
            if (temp == 0) {
                k = false;
                break;
            }
        }
        return k;
    }
}