package com.github.tasks.loops;
import java.util.Scanner;

public class Loop6 {
    public static void main(String[] args) {
        Scanner num = new Scanner(System.in);
        int n;
        System.out.print("Enter num n: ");
        n = num.nextInt();
        mirr(n);
    }

    public static void mirr(int n) {
        do {
            System.out.print(n % 10);
            n /= 10;
        } while (n > 0);
    }
}
