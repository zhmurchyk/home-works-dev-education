package com.github.tests.conditionaloperatorstests;

import com.github.tasks.conditionaloperators.CO2;
import org.junit.Assert;
import org.junit.Test;

public class CO2Test {

    @Test
    public void checkQuarter1Test(){
        String exp = "point in first quarter";
        String act = CO2.checkQuarter(4, 6);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void checkQuarter2Test(){
        String exp = "point in second quarter";
        String act = CO2.checkQuarter(-2, 4);
        Assert.assertEquals(exp, act);
    }

    @Test
    public  void checkQuarter3Test(){
        String exp = "point in third quarter";
        String act = CO2.checkQuarter(-4, -6);
        Assert.assertEquals(exp, act);
    }

    @Test
    public  void checkQuarter4Test(){
        String exp = "point in fourth quarter";
        String act = CO2.checkQuarter(4, -16);
        Assert.assertEquals(exp, act);
    }

    @Test
    public  void checkAxisTest(){
        String exp = "point on the coordinate axis";
        String act = CO2.checkQuarter(0, 6);
        Assert.assertEquals(exp, act);
    }
}