package com.github.tests.conditionaloperatorstests;

import com.github.tasks.conditionaloperators.CO1;
import org.junit.Assert;
import org.junit.Test;

public class CO1Test {

    @Test
    public void culcEven() {
        String exp = "a * b = 12.0";
        String act = CO1.calc(4, 3);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void culcOdd() {
        String exp = "a + b = 8.0";
        String act = CO1.calc(5, 3);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void culcZero() {
        String exp = "a * b = 0.0";
        String act = CO1.calc(0, 0);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void multipleResultBig() {
        String exp = "a * b = 1500.0";
        String act = CO1.calc(500, 3);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void multipleResultOne() {
        String exp = "a + b = 2.0";
        String act = CO1.calc(1, 1);
        Assert.assertEquals(exp, act);
    }
}