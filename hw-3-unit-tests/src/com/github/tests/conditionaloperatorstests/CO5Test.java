package com.github.tests.conditionaloperatorstests;

import com.github.tasks.conditionaloperators.CO5;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class CO5Test {

    protected ByteArrayOutputStream output;
    private PrintStream old;

    @Before
    public void setUpStreams() {
        old = System.out;
        output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(old);
    }

    @Test
    public void checkRankOkTest() {
        String exp = "Mark A";
        CO5.checkRank(95);
        Assert.assertEquals(exp, output.toString());
    }

    @Test
    public void checkRankWrongTest() {
        String exp = "Wrong rank";
        CO5.checkRank(-12);
        Assert.assertEquals(exp, output.toString());
    }
}