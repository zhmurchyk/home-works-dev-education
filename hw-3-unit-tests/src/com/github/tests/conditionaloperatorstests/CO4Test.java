package com.github.tests.conditionaloperatorstests;

import com.github.tasks.conditionaloperators.CO4;
import org.junit.Assert;
import org.junit.Test;

public class CO4Test {

    @Test
    public void maxIfSumTest(){
        int exp = 6;
        int act = CO4.maxIf(1, 1, 1);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void maxIfMultTest(){
        int exp = 19;
        int act = CO4.maxIf(2, 2,4);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void maxIfZeroTest(){
        int exp = 3;
        int act = CO4.maxIf(0, 0,0);
        Assert.assertEquals(exp, act);
    }
}