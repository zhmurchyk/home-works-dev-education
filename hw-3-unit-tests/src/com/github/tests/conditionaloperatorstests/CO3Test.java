package com.github.tests.conditionaloperatorstests;

import com.github.tasks.conditionaloperators.CO3;
import org.junit.Assert;
import org.junit.Test;

public class CO3Test {

    @Test
    public void sumPosTest() {
        int exp = 20;
        int act = CO3.sum(15, 2, 3);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sumNegTest() {
        int exp = 0;
        int act = CO3.sum(-14, -6, -1);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sumZeroTest() {
        int exp = 0;
        int act = CO3.sum(0, 0, 0);
        Assert.assertEquals(exp, act);
    }
}