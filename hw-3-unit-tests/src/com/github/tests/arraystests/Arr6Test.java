package com.github.tests.arraystests;

import com.github.tasks.arrays.Arr6;
import org.junit.Assert;
import org.junit.Test;

public class Arr6Test {

    @Test
    public void revElTest() {
        int [] arr = {12, 5, 2, 10};
        int [] exp = {10, 2, 5, 12};
        int [] act = Arr6.rev(4, arr);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void revElOne() {
        int [] arr = {12};
        int [] exp = {12};
        int [] act = Arr6.rev(1, arr);
        Assert.assertArrayEquals(exp, act);
    }
}