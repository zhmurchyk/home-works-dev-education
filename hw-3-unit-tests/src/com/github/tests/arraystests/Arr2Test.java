package com.github.tests.arraystests;

import com.github.tasks.arrays.Arr2;
import org.junit.Assert;
import org.junit.Test;

public class Arr2Test {

    @Test
    public void maxElTest() {
        int [] arr = {12, 5, 2, 10};
        int exp = 12;
        int act = Arr2.maxEl(4, arr);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void maxElOne() {
        int [] arr = {12};
        int exp = 12;
        int act = Arr2.maxEl(1, arr);
        Assert.assertEquals(exp, act);
    }
}