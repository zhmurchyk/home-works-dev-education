package com.github.tests.arraystests;

import com.github.tasks.arrays.Arr3;
import org.junit.Assert;
import org.junit.Test;

public class Arr3Test {

    @Test
    public void minElIndexTest() {
        int [] arr = {12, 5, 2, 10};
        int exp = 2;
        int act = Arr3.minElIndex(4, arr);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void minElIndexOne() {
        int [] arr = {12};
        int exp = 0;
        int act = Arr3.minElIndex(1, arr);
        Assert.assertEquals(exp, act);
    }
}