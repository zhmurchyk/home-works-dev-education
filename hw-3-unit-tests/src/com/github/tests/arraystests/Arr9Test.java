package com.github.tests.arraystests;

import com.github.tasks.arrays.Arr9;
import org.junit.Assert;
import org.junit.Test;

public class Arr9Test {

    @Test
    public void bubbleSortTest() {
        int [] arr = {12, 5, 2, 10};
        int [] exp = {2, 5, 10, 12};
        int [] act = Arr9.bubbleSort(4, arr);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void bubbleSortOneTest() {
        int [] arr = {12};
        int [] exp = {12};
        int [] act = Arr9.bubbleSort(1, arr);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void selectSortTest() {
        int [] arr = {12, 5, 2, 10};
        int [] exp = {2, 5, 10, 12};
        int [] act = Arr9.selectSort(4, arr);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void selectSortOneTest() {
        int [] arr = {12};
        int [] exp = {12};
        int [] act = Arr9.selectSort(1, arr);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void insertSortTest() {
        int [] arr = {12, 5, 2, 10};
        int [] exp = {2, 5, 10, 12};
        int [] act = Arr9.insertSort(4, arr);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void insertSortOneTest() {
        int [] arr = {12};
        int [] exp = {12};
        int [] act = Arr9.insertSort(1, arr);
        Assert.assertArrayEquals(exp, act);
    }
}