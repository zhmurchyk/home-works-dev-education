package com.github.tests.arraystests;

import com.github.tasks.arrays.Arr4;
import org.junit.Assert;
import org.junit.Test;

public class Arr4Test {

    @Test
    public void maxElIndexTest() {
        int [] arr = {12, 5, 2, 10};
        int exp = 0;
        int act = Arr4.maxElIndex(4, arr);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void maxElIndexOne() {
        int [] arr = {12};
        int exp = 0;
        int act = Arr4.maxElIndex(1, arr);
        Assert.assertEquals(exp, act);
    }
}