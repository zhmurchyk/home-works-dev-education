package com.github.tests.arraystests;

import com.github.tasks.arrays.Arr5;
import org.junit.Assert;
import org.junit.Test;

public class Arr5Test {

    @Test
    public void sumElTest() {
        int [] arr = {12, 5, 2, 10};
        int exp = 15;
        int act = Arr5.sum(4, arr);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sumElOne() {
        int [] arr = {12};
        int exp = 0;
        int act = Arr5.sum(1, arr);
        Assert.assertEquals(exp, act);
    }
}