package com.github.tests.arraystests;

import com.github.tasks.arrays.Arr8;
import org.junit.Assert;
import org.junit.Test;

public class Arr8Test {

    @Test
    public void replaceElTest() {
        int [] arr = {12, 5, 2, 10};
        int [] exp = {2, 10, 12, 5};
        int [] act = Arr8.replace(4, arr);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void replaceElTwo() {
        int [] arr = {12, 4};
        int [] exp = {4, 12};
        int [] act = Arr8.replace(2, arr);
        Assert.assertArrayEquals(exp, act);
    }
}