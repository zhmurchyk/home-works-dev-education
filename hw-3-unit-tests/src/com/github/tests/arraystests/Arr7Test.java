package com.github.tests.arraystests;

import com.github.tasks.arrays.Arr7;
import org.junit.Assert;
import org.junit.Test;

public class Arr7Test {

    @Test
    public void countOddTest() {
        int [] arr = {12, 5, 3, 10};
        int exp = 2;
        int act = Arr7.countOdd(4, arr);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void countOddZero() {
        int [] arr = {12};
        int exp = 0;
        int act = Arr7.countOdd(1, arr);
        Assert.assertEquals(exp, act);
    }
}