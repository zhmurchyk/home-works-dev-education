package com.github.tests.arraystests;

import com.github.tasks.arrays.Arr1;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class Arr1Test {

    @Test
    public void minElTest() {
        int [] arr = {12, 5, 2, 10};
        int exp = 2;
        int act = Arr1.minEl(4, arr);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void minElOne() {
        int [] arr = {12};
        int exp = 12;
        int act = Arr1.minEl(1, arr);
        Assert.assertEquals(exp, act);
    }
}