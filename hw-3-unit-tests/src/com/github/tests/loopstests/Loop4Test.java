package com.github.tests.loopstests;

import com.github.tasks.loops.Loop4;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class Loop4Test {

    @Test
    public void fact() {
        int exp = 120;
        int act = Loop4.fact(5);
        Assert.assertEquals(exp, act);
    }
}