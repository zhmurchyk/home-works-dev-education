package com.github.tests.loopstests;

import com.github.tasks.loops.Loop1;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class Loop1Test {

    @Test
    public void sum() {
        int exp = 2450;
        int act = Loop1.sum();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void num() {
        int exp = 49;
        int act = Loop1.num();
        Assert.assertEquals(exp, act);
    }
}