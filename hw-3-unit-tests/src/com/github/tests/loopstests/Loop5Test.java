package com.github.tests.loopstests;

import com.github.tasks.loops.Loop5;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class Loop5Test {

    @Test
    public void sumPoz() {
        int exp = 21;
        int act = Loop5.sum(654321);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sum0() {
        int exp = 0;
        int act = Loop5.sum(0);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sumNeg() {
        int exp = 15;
        int act = Loop5.sum(-54321);
        Assert.assertEquals(exp, act);
    }
}