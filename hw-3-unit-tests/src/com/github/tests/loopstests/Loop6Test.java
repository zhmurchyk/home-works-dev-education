package com.github.tests.loopstests;

import com.github.tasks.loops.Loop6;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Loop6Test {

    protected ByteArrayOutputStream output;
    private PrintStream old;

    @Before
    public void setUpStreams() {
        old = System.out;
        output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(old);
    }

    @Test
    public void mirr123() {
        String exp = "321";
        Loop6.mirr(123);
        Assert.assertEquals(exp, output.toString());
    }

    @Test
    public void mirr1() {
        String exp = "1";
        Loop6.mirr(1);
        Assert.assertEquals(exp, output.toString());
    }

    @Test
    public void mirr0() {
        String exp = "0";
        Loop6.mirr(0);
        Assert.assertEquals(exp, output.toString());
    }
}