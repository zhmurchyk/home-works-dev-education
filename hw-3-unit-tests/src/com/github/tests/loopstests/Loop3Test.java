package com.github.tests.loopstests;

import com.github.tasks.loops.Loop3;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class Loop3Test {

    @Test
    public void root1() {
        int exp = 7;
        int act = Loop3.root1(50);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void root2() {
        int exp = 7;
        int act = Loop3.root2(50);
        Assert.assertEquals(exp, act);
    }
}