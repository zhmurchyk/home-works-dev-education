package com.github.tests.loopstests;

import com.github.tasks.loops.Loop2;
import org.junit.Assert;
import org.junit.Test;

public class Loop2Test {

    @Test
    public void isPrimeTrue() {
        boolean exp = true;
        boolean act = Loop2.isPrime(13);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void isPrimeFalse() {
        boolean exp = false;
        boolean act = Loop2.isPrime(12);
        Assert.assertEquals(exp, act);
    }
}