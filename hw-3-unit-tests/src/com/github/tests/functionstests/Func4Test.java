package com.github.tests.functionstests;

import com.github.tasks.functions.Func4;
import org.junit.Assert;
import org.junit.Test;

public class Func4Test {

    @Test
    public void distTest(){
        double exp = 5.0;
        double act = Func4.dist(2,1,6,4);
        Assert.assertEquals(exp, act, 0.01);
    }

    @Test
    public void distZeroTest(){
        double exp = 0.0;
        double act = Func4.dist(0,0,0,0);
        Assert.assertEquals(exp, act, 0.01);
    }
}