package com.github.tests.functionstests;

import com.github.tasks.functions.Func2;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Func2Test {

    protected ByteArrayOutputStream output;
    private PrintStream old;

    @Before
    public void setUpStreams() {
        old = System.out;
        output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(old);
    }

    @Test
    public void toStrTest() {
        String exp = "триста пятьдесят два";
        Func2.toStr(352);
        Assert.assertEquals(exp, output.toString());
    }

    @Test
    public void toStr100Test() {
        String exp = "сто";
        Func2.toStr(100);
        Assert.assertEquals(exp, output.toString());
    }
}