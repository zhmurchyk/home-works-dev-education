package com.github.tests.functionstests;

import com.github.tasks.functions.Func1;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Func1Test {

    protected ByteArrayOutputStream output;
    private PrintStream old;

    @Before
    public void setUpStreams() {
        old = System.out;
        output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(old);
    }

    @Test
    public void dayOkTest() {
        String exp = "SUN";
        Func1.func(7, new String[]{"MON", "TUE", "WED", "THU", "FRA", "SAT", "SUN"});
        Assert.assertEquals(exp, output.toString());
    }

    @Test
    public void dayWrongTest() {
        String exp = "Wrong number of day of week!!!";
        Func1.func(12, new String[]{"MON", "TUE", "WED", "THU", "FRA", "SAT", "SUN"});
        Assert.assertEquals(exp, output.toString());
    }
}