package com.github.conversion;

public class Conversion {

    public static void main(String[] args) {

        System.out.println("Integer to String: " + convertIntToString(123));
        System.out.println("Real number to String: " + convertRealToString( 123.321));
        System.out.println("String to Integer: " + convertStringToInt("123"));
        System.out.println("String to Real number: " + convertStringToReal("123.321"));
    }

    public static String convertIntToString(int num) {
        return Integer.toString(num);
    }

    public static String convertRealToString(Object num) {
        return String.valueOf(num);
    }

    public static int convertStringToInt(String num) {
        if(num == null) {
            throw new NumberFormatException ("String is null!");
        }
        if(!num.matches("(-*)(\\d+)")) {
            throw new NumberFormatException("String hasn't number!");
        }
        return Integer.parseInt(num);
    }

    public static double convertStringToReal(String num) {
        if(num == null) {
            throw new NumberFormatException ("String is null!");
        }
        if(!num.matches("(-*)(\\d+)\\.(\\d+)")) {
            throw new NumberFormatException("String hasn't number!");
        }
        return Double.parseDouble(num);
    }
}
