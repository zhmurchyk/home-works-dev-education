package com.github.oneLine;

public class OneLine {

    public static void main(String[] args) {

        printLine('A', 'Z');
        printRevLine('z', 'a');
        printLine('а', 'я');
        printLine('0', '9');
        printLine((char) 32, (char) 127);


    }

    public static void printLine(char start, char finish) {
        for(int i = start; i < finish+1; i++) {
            System.out.print((char) i + " ");
        }
        System.out.println();
    }

    public static void printRevLine(char start, char finish) {
        for(int i = start; i > finish-1; i--) {
            System.out.print((char) i + " ");
        }
        System.out.println();
    }
}
