package com.github.strFunctions;

import java.util.Arrays;

public class StrFunc {

    public static void main(String[] args) {

        String str = "London is the capital of Great Britain,its political,economic and cultural centre!!!";
        System.out.println("Min word has " + minWord(str) + " letters.");

        String[] words = {"asd", "dsas", "gf", "qwee", "gfdgd", "hgf", "bcvs"};
        System.out.println(Arrays.toString(dollarSign(words, 4)));

        System.out.println("New sentence with spaces: " + addSpace(str));

        System.out.println("New sentence without repeated char: " + noRepeatedChar(str));

        System.out.println("Number of words in sentence: " + numberOfWords(str));

        System.out.println("New string: " + deletePart(str, 10, 10));

        System.out.println("Reverse string: " + reverseString(str));

        System.out.println("New string without last word: " + deleteLastWord(str));
    }

    public static int minWord(String str) {
        if(str == null) {
            throw new NumberFormatException ("String is null!");
        }
        str = str.replaceAll("\\p{Punct}", "");
        String[] words = str.split(" ");
        int min = words[0].length();
        for (String word : words) {
            if(word.length() < min) {
                min = word.length();
            }
        }
        return min;
    }

    public static String[] dollarSign(String[] words, int length) {
        if(words == null) {
            throw new NullPointerException ("Array of words is null!");
        }
        for(int i = 0; i < words.length; i++) {
            if(words[i].length() == length) {
                words[i] = words[i].replaceAll(".{3}$", "\\$\\$\\$");
            }
        }
        return words;
    }

    public static String addSpace(String str) {
        if(str == null) {
            throw new NullPointerException ("String is null!");
        }
        str = str.replaceAll("(?<=[,.:!?])(?=\\S)", " ");
        return str;
    }

    public static String noRepeatedChar(String sentence) {
        if(sentence == null) {
            throw new NullPointerException ("String is null!");
        }
        StringBuilder sb = new StringBuilder();
        sentence.chars().distinct().forEach(c -> sb.append((char) c));
        return sb.toString();
    }

    public static int numberOfWords(String sentence) {
        if (sentence == null) {
            return 0;
        }
        String[] words = sentence.split("[\\pP\\s&&[^']]+");
        return words.length;
    }

    public static String deletePart(String sentence, int start, int length) {
        if(sentence == null) {
            throw new NullPointerException ("String is null!");
        }
        if(start < 0 ||length < 0) {
            return sentence;
        }
        return sentence.substring(0, start) + sentence.substring(start+length);
    }

    public static String reverseString (String sentence) {
        if(sentence == null) {
            throw new NullPointerException ("String is null!");
        }
        StringBuilder sb = new StringBuilder(sentence);
        return sb.reverse().toString();
    }

    public static String deleteLastWord(String sentence) {
        if(sentence == null) {
            throw new NullPointerException ("String is null!");
        }
        String[] words = sentence.trim().split("[\\pP\\s&&[^']]+");
        String[] newWords = new String[words.length-1];
        for(int i = 0; i < newWords.length; i++) {
            newWords[i] = words[i];
        }
        return String.join(" ", newWords);
    }
}
