package com.github.drawing;

public class Drawing8 {
    public static void main(String[] args) {
        String[][] arrStr = new String[7][7];
        for(int i = 0; i < arrStr.length; i++){
            for(int j = 0; j < arrStr[i].length; j++){
                if (i == 0 || i == j && j < 4 || i + j == 6 && i < 4)
                    arrStr[i][j] = "*";
                else
                    arrStr[i][j] = " ";
                System.out.print(arrStr[i][j]+"  ");
            }
            System.out.println("");
        }
    }
}
