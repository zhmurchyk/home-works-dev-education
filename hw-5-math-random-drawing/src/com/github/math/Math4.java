package com.github.math;

import java.util.Scanner;

public class Math4 {
    public static void main(String[] args) {
        double x;
        Scanner in = new Scanner(System.in);
        System.out.println("Enter x: ");
        x = in.nextDouble();
        double z = 6 * Math.log(Math.sqrt(Math.exp(x+1)+2*Math.exp(x)*Math.cos(x)))/
                Math.log(x-Math.exp(x+1)*Math.sin(x)) + Math.abs(Math.cos(x)/Math.exp(Math.sin(x)));
        System.out.println(z);
    }
}
