package com.github.math;

import java.util.Scanner;

public class Math1 {
    public static void main(String[] args) {
        float alpha, speed;
        Scanner in = new Scanner(System.in);
        System.out.println("Enter angle: ");
        alpha = in.nextFloat();
        System.out.println("Enter speed: ");
        speed = in.nextFloat();
        double s = dist(alpha, speed);
        System.out.println("Distance of shoot is: \n" + s + " meters");
    }

    public static double dist(float a, float spd){
        spd = spd * 1000 / 3600;
        double s = ((spd * spd) / 9.8) * Math.sin(Math.toRadians(2 * a));
        return s;
    }
}
