package com.github.math;

import java.util.Scanner;

public class Math2 {
    public static void main(String[] args) {
        float t, s;
        int  v1, v2;
        Scanner in = new Scanner(System.in);
        System.out.println("Enter speed1: ");
        v1 = in.nextInt();
        System.out.println("Enter speed2: ");
        v2 = in.nextInt();
        System.out.println("Enter start distance: ");
        s = in.nextFloat();
        System.out.println("Enter time: ");
        t = in.nextFloat();
        s = Math.addExact(v1, v2) * t + s;
        System.out.println("Distance between cars is:\n " + s + " kilometers");
    }
}
