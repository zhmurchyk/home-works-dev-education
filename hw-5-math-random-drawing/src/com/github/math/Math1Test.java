package com.github.math;

import org.junit.Test;

import static org.junit.Assert.*;

public class Math1Test {

    @Test
    public void math1(){
        float a = 45;
        float spd = 100;
        double expRes = 78.735;
        double res = Math1.dist(a, spd);
        assertEquals(expRes, res);
    }

}