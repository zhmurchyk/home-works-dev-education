package com.github.converter;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.HashMap;

public class TimeConverter {
    private JButton myButton;
    private JLabel titleLabel;
    private JLabel centerLabel;
    private JLabel bLabel;
    private JTextField frField;
    private JComboBox<String> fromBox;
    private JTextField toField;
    private JComboBox<String> toBox;
    private final String[] lengthTypes = {"Sec", "Min", "Hour", "Day", "Week", "Month", "Astronomical Year"};
    private final HashMap<String, HashMap<String, Double>> converter = new HashMap<>();

    public JPanel convert() {

        JPanel firstPanel = new JPanel();
        firstPanel.setLayout(null);
        firstPanel.setSize(400, 400);
        firstPanel.setVisible(true);

        titleLabel = new JLabel("Time Converter");
        titleLabel.setBounds(120, 5, 250, 20);
        firstPanel.add(titleLabel);

        frField = new JTextField(20);
        frField.setBounds(20, 35, 300, 30);
        firstPanel.add(frField);

        centerLabel = new JLabel("From:");
        centerLabel.setBounds(20, 85, 100, 30);
        firstPanel.add(centerLabel);

        fromBox = new JComboBox<>(lengthTypes);
        fromBox.setBounds(70, 85, 250, 30);
        firstPanel.add(fromBox);

        bLabel = new JLabel("To:");
        bLabel.setBounds(20, 130, 100, 30);
        firstPanel.add(bLabel);

        toBox = new JComboBox<>(lengthTypes);
        toBox.setBounds(70, 130, 250, 30);
        firstPanel.add(toBox);

        myButton = new JButton("CONVERT");
        myButton.setBounds(20, 180, 300, 50);
        firstPanel.add(myButton);
        myButton.addActionListener(new Action());

        toField = new JTextField(20);
        toField.setBounds(20, 250, 300, 30);
        toField.setEnabled(false);
        firstPanel.add(toField);

        return firstPanel;
    }

    public TimeConverter() {

        converter.put("Sec", new HashMap<>());
        converter.get("Sec").put("Sec", 1.0);
        converter.get("Sec").put("Min", 0.01667);
        converter.get("Sec").put("Hour", 0.0002778);
        converter.get("Sec").put("Day", 0.00001157);
        converter.get("Sec").put("Week", 0.000001653);
        converter.get("Sec").put("Month", 0.0000003803);
        converter.get("Sec").put("Astronomical Year", 0.00000003169);

        converter.put("Min", new HashMap<>());
        converter.get("Min").put("Min", 1.0);
        converter.get("Min").put("Sec", 60.0);
        converter.get("Min").put("Hour", 0.01667);
        converter.get("Min").put("Day", 0.0006944);
        converter.get("Min").put("Week", 0.00009921);
        converter.get("Min").put("Month", 0.00002282);
        converter.get("Min").put("Astronomical Year", 0.000001901);

        converter.put("Hour", new HashMap<>());
        converter.get("Hour").put("Hour", 1.0);
        converter.get("Hour").put("Sec", 3600.0);
        converter.get("Hour").put("Min", 60.0);
        converter.get("Hour").put("Day", 0.04167);
        converter.get("Hour").put("Week", 0.005952);
        converter.get("Hour").put("Month", 0.001369);
        converter.get("Hour").put("Astronomical Year", 0.0001141);

        converter.put("Day", new HashMap<>());
        converter.get("Day").put("Day", 1.0);
        converter.get("Day").put("Sec", 86400.0);
        converter.get("Day").put("Min", 1440.0);
        converter.get("Day").put("Hour", 24.0);
        converter.get("Day").put("Week", 0.1429);
        converter.get("Day").put("Month", 0.03285);
        converter.get("Day").put("Astronomical Year", 0.002738);

        converter.put("Week", new HashMap<>());
        converter.get("Week").put("Week", 1.0);
        converter.get("Week").put("Sec", 604800.0);
        converter.get("Week").put("Min", 10080.0);
        converter.get("Week").put("Hour", 168.0);
        converter.get("Week").put("Day", 7.0);
        converter.get("Week").put("Month", 0.23);
        converter.get("Week").put("Astronomical Year", 0.01916);

        converter.put("Month", new HashMap<>());
        converter.get("Month").put("Month", 1.0);
        converter.get("Month").put("Sec", 2629744.0);
        converter.get("Month").put("Min", 43829.0);
        converter.get("Month").put("Hour", 730.5);
        converter.get("Month").put("Day", 30.44);
        converter.get("Month").put("Week", 4.348);
        converter.get("Month").put("Astronomical Year", 0.08333);

        converter.put("Astronomical Year", new HashMap<>());
        converter.get("Astronomical Year").put("Astronomical Year", 1.0);
        converter.get("Astronomical Year").put("Sec", 31557600.0);
        converter.get("Astronomical Year").put("Min", 525960.0);
        converter.get("Astronomical Year").put("Hour", 8766.0);
        converter.get("Astronomical Year").put("Day", 365.3);
        converter.get("Astronomical Year").put("Week", 52.18);
        converter.get("Astronomical Year").put("Month", 12.0);
    }

    class Action implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Double numToConvert = Double.parseDouble(frField.getText());
            Double conversionRate = converter.get(fromBox.getSelectedItem()).get(toBox.getSelectedItem());
            String result = new DecimalFormat("#0.000000000").format(numToConvert * conversionRate);
            toField.setText(result);
        }
    }
}
