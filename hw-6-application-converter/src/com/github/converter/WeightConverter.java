package com.github.converter;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.HashMap;

public class WeightConverter {
    private JButton myButton;
    private JLabel titleLabel;
    private JLabel centerLabel;
    private JLabel bLabel;
    private JTextField frField;
    private JComboBox<String> fromBox;
    private JTextField toField;
    private JComboBox<String> toBox;
    private final String[] lengthTypes = {"KG", "G", "Carat", "Eng pound", "Pound", "Stone", "Rus pound"};
    private final HashMap<String, HashMap<String, Double>> converter = new HashMap<>();

    public JPanel convert() {

        JPanel firstPanel = new JPanel();
        firstPanel.setLayout(null);
        firstPanel.setSize( 400, 400);

        firstPanel.setVisible(true);

        titleLabel = new JLabel("Weight Converter");
        titleLabel.setBounds(120, 5, 250, 20);
        firstPanel.add(titleLabel);

        frField = new JTextField(20);
        frField.setBounds(20, 35, 300, 30);
        firstPanel.add(frField);

        centerLabel = new JLabel("From:");
        centerLabel.setBounds(20, 85, 100, 30);
        firstPanel.add(centerLabel);

        fromBox = new JComboBox<>(lengthTypes);
        fromBox.setBounds(70, 85, 250, 30);
        firstPanel.add(fromBox);

        bLabel = new JLabel("To:");
        bLabel.setBounds(20, 130, 100, 30);
        firstPanel.add(bLabel);

        toBox = new JComboBox<>(lengthTypes);
        toBox.setBounds(70, 130, 250, 30);
        firstPanel.add(toBox);

        myButton = new JButton("CONVERT");
        myButton.setBounds(20, 180, 300, 50);
        firstPanel.add(myButton);
        myButton.addActionListener(new SubmitListener());

        toField = new JTextField(20);
        toField.setBounds(20, 250, 300, 30);
        toField.setEnabled(false);
        firstPanel.add(toField);

        return firstPanel;
    }

    public WeightConverter() {

        converter.put("KG", new HashMap<>());
        converter.get("KG").put("KG", 1.0);
        converter.get("KG").put("G", 1000.0);
        converter.get("KG").put("Carat", 5000.0);
        converter.get("KG").put("Eng pound", 2.2046226);
        converter.get("KG").put("Pound", 2.6792289);
        converter.get("KG").put("Stone", 0.15747304);
        converter.get("KG").put("Rus pound", 2.4419307);

        converter.put("G", new HashMap<>());
        converter.get("G").put("G", 1.0);
        converter.get("G").put("KG", 0.001);
        converter.get("G").put("Carat", 5.0);
        converter.get("G").put("Eng pound", 0.002204623);
        converter.get("G").put("Pound", 0.002679229);
        converter.get("G").put("Stone", 0.000157473);
        converter.get("G").put("Rus pound", 0.002441931);

        converter.put("Carat", new HashMap<>());
        converter.get("Carat").put("Carat", 1.0);
        converter.get("Carat").put("KG", 0.0002);
        converter.get("Carat").put("G", 0.2);
        converter.get("Carat").put("Eng pound", 0.000440925);
        converter.get("Carat").put("Pound", 0.000535846);
        converter.get("Carat").put("Stone", 0.000031495);
        converter.get("Carat").put("Rus pound", 0.000488386);

        converter.put("Eng pound", new HashMap<>());
        converter.get("Eng pound").put("Eng pound", 1.0);
        converter.get("Eng pound").put("KG", 0.453592374);
        converter.get("Eng pound").put("G", 453.592374495);
        converter.get("Eng pound").put("Carat", 2267.961872475);
        converter.get("Eng pound").put("Pound", 1.215277799);
        converter.get("Eng pound").put("Stone", 0.07142857);
        converter.get("Eng pound").put("Rus pound", 1.107641143);

        converter.put("Pound", new HashMap<>());
        converter.get("Pound").put("Pound", 1.0);
        converter.get("Pound").put("KG", 0.373241719);
        converter.get("Pound").put("G", 373.241719);
        converter.get("Pound").put("Carat", 1866.208595);
        converter.get("Pound").put("Eng pound", 0.822857129);
        converter.get("Pound").put("Stone", 0.058775508);
        converter.get("Pound").put("Rus pound", 0.911430412);

        converter.put("Stone", new HashMap<>());
        converter.get("Stone").put("Stone", 1.0);
        converter.get("Stone").put("KG", 6.350293358);
        converter.get("Stone").put("G", 6350.293358152);
        converter.get("Stone").put("Carat", 31751.46679);
        converter.get("Stone").put("Eng pound", 14.000000254);
        converter.get("Stone").put("Pound", 17.013889489);
        converter.get("Stone").put("Rus pound", 15.506976306);

        converter.put("Rus pound", new HashMap<>());
        converter.get("Rus pound").put("Rus pound", 1.0);
        converter.get("Rus pound").put("KG", 0.409512031);
        converter.get("Rus pound").put("G", 409.512031);
        converter.get("Rus pound").put("Carat", 2047.560153939);
        converter.get("Rus pound").put("Eng pound", 0.902819478);
        converter.get("Rus pound").put("Pound", 1.097176468);
        converter.get("Rus pound").put("Stone", 0.064487104);
    }

    class SubmitListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Double numToConvert = Double.parseDouble(frField.getText());
            Double conversionRate = converter.get(fromBox.getSelectedItem()).get(toBox.getSelectedItem());
            String result = new DecimalFormat("#0.000000000").format(numToConvert * conversionRate);
            toField.setText(result);
        }
    }
}
