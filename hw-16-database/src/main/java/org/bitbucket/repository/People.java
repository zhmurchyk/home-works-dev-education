package org.bitbucket.repository;

import java.sql.SQLException;

public class People extends BaseTable implements TableOperations{
    public People() throws SQLException {
        super("people");
    }

    @Override
    public void createTable() throws SQLException {
        super.executeSqlStatement("CREATE TABLE IF NOT EXISTS people(" +
                "id INT AUTO_INCREMENT PRIMARY KEY," +
                "firstName VARCHAR(32) NOT NULL," +
                "lastName VARCHAR(32) NOT NULL," +
                "age INT NOT NULL," +
                "id_street INT DEFAULT NULL)", "Создана таблица " + tableName);
    }

    @Override
    public void createForeignKeys() throws SQLException {
        super.executeSqlStatement(" ALTER TABLE people ADD FOREIGN KEY (id_street) REFERENCES streets(id)",
                "Cоздан внешний ключ people.id_street -> streets.id");
    }
}
