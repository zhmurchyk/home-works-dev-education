package org.bitbucket.repository;

import java.sql.SQLException;

public class Streets extends BaseTable implements TableOperations{

    public Streets() throws SQLException {
        super("streets");
    }

    @Override
    public void createTable() throws SQLException {
        super.executeSqlStatement("CREATE TABLE IF NOT EXISTS streets(" +
                "id INT AUTO_INCREMENT PRIMARY KEY," +
                "name VARCHAR(32) NOT NULL)", "Создана таблица " + tableName);
//        super.executeSqlStatement("CREATE TABLE IF NOT EXISTS streets (" +
//                "id INT AUTO_INCREMENT PRIMARY KEY," +
//               "name VARCHAR(50) NOT NULL)"
//        );
    }

    @Override
    public void createForeignKeys() throws SQLException {
    }
}
