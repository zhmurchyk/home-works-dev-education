package org.bitbucket;

import org.bitbucket.repository.People;
import org.bitbucket.repository.Streets;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class AddressBookDB {
//    public static final String DB_URL = "jdbc:h2:/c:/Git/home-works-dev-education/hw-16-database/src/main/java/org/bitbucket/db/addressBook";
    public static final String DB_URL = "jdbc:h2:tcp://localhost/~/test";
    public static final String DB_Driver = "org.h2.Driver";

    static final String USER = "sa";
    static final String PASS = "";

    // Таблицы СУБД
    Streets streets;
    People people;

    // Получить новое соединение с БД
    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(DB_URL,USER,PASS);
    }

    // Инициализация
    public AddressBookDB() throws SQLException, ClassNotFoundException {
        Class.forName(DB_Driver);
        // Инициализируем таблицы
        streets = new Streets();
        people = new People();
    }

    public void createTablesAndForeignKeys() throws SQLException {
        streets.createTable();
        people.createTable();
        // Создание внешних ключей (связь между таблицами)
        people.createForeignKeys();
    }

    public static void main(String[] args) {
        try{
            AddressBookDB addressBookDB = new AddressBookDB();
            addressBookDB.createTablesAndForeignKeys();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Ошибка SQL !");
        } catch (ClassNotFoundException e) {
            System.out.println("JDBC драйвер для СУБД не найден!");
        }
    }
}
