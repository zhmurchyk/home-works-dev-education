package org.bitbucket.model;

public class Street extends BaseModel{
    private String name;

    public Street() {
    }

    public Street(long id, String name) {
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
