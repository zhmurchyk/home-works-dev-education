package org.bitbucket.model;

// Модель Person

public class Person extends BaseModel{
    private String firsName;
    private String lastName;
    private int age;
    private long id_street;

    public Person() {
    }

    public Person(long id, String firsName, String lastName, int age, long id_street) {
        super(id);
        this.firsName = firsName;
        this.lastName = lastName;
        this.age = age;
        this.id_street = id_street;
    }

    public String getFirsName() {
        return firsName;
    }

    public void setFirsName(String firsName) {
        this.firsName = firsName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public long getId_street() {
        return id_street;
    }

    public void setId_street(long street) {
        this.id_street = id_street;
    }



}
