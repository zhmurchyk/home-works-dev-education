package com.github.collections.list;

public class ArrayListGen<T extends Comparable<?>> implements IListGen<T> {

    private Object[] array = new Object[10];

    private int index;

    @Override
    public void init(T[] init) {
        if (init == null) {
            this.index = 0;
        } else {
            this.index = init.length;
            for (int i = 0; i < this.index; i++) {
                this.array[i] = init[i];
            }
        }
    }

    @Override
    public void clear() {
        this.index = 0;
    }

    @Override
    public int size() {
        return this.index;
    }

    @Override
    public <T> T[] toArray() {
        Object[] result = new Object[this.index];
        for (int i = 0; i < this.index; i++) {
            result[i] = array[i];
        }
        return (T[]) result;
    }

    @Override
    public void addStart(T val) {
        for (int i = this.index; i > 0 ; i--) {
            this.array[i] = this.array[i - 1];
        }
        this.array[0] = val;
        this.index++;
    }

    @Override
    public void addStart(T val, Comparable<?> com) {

    }

    @Override
    public void addEnd(T val) {

    }

    @Override
    public void addByPos(int pos, T val) {

    }

    @Override
    public T removeStart() {
        return null;
    }

    @Override
    public T removeEnd() {
        return null;
    }

    @Override
    public T removeByPos(int pos) {
        return null;
    }

    @Override
    public T max() {
        // TODO: 17.03.21 iterator?
        return null;
    }

    @Override
    public T min() {
        return null;
    }

    @Override
    public int maxPos() {
        return 0;
    }

    @Override
    public int minPos() {
        return 0;
    }

    @Override
    public T[] sort() {
        return null;
    }

    @Override
    public T get(int pos) {
        return null;
    }

    @Override
    public T[] halfRevers() {
        return null;
    }

    @Override
    public T[] revers() {
        return null;
    }

    @Override
    public void set(int pos, T val) {

    }
}
