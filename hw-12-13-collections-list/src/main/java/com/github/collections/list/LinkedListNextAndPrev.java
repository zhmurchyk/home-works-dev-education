package com.github.collections.list;

import java.util.Objects;

public class LinkedListNextAndPrev implements IList{

    private Node start;

    private Node end;

    public static class Node {

        int value;

        Node next;

        Node prev;

        public Node(int value) {
            this.value = value;
        }
    }

    @Override
    public void init(int[] init) {
        if (Objects.nonNull(init)) {
            for (int i = init.length - 1; i >= 0 ; i--) {
                addStart(init[i]);
            }
        }
    }

    @Override
    public void clear() {
        this.start = this.end = null;
    }

    @Override
    public int size() {
        int count = 0;
        Node tmp = this.start;
        while (tmp != null) {
            count++;
            tmp = tmp.next;
        }
        return count;
    }

    @Override
    public int[] toArray() {
        int count = 0;
        int[] result = new int[size()];
        Node tmp = this.start;
        while (tmp != null) {
            result[count++] = tmp.value;
            tmp = tmp.next;
        }
        return result;
    }

    @Override
    public void addStart(int val) {
        if (Objects.isNull(this.start)) {
            this.start = this.end = new Node(val);
        }
        Node tmp = new Node(val);
        tmp.next = this.start;
        this.start = tmp;
        tmp.next.prev = tmp;
    }

    @Override
    public void addEnd(int val) {
        if (Objects.isNull(this.start)) {
            this.start = this.end = new Node(val);
        }
        Node tmp = new Node(val);
        this.end.next = tmp;
        tmp.prev = end;
        this.end = tmp;
    }

    @Override
    public void addByPos(int pos, int val) {

    }

    @Override
    public void removeStart() {

    }

    @Override
    public void removeEnd() {

    }

    @Override
    public void removeByPos(int pos) {

    }

    @Override
    public int max() {
        return 0;
    }

    @Override
    public int min() {
        return 0;
    }

    @Override
    public int maxPos() {
        return 0;
    }

    @Override
    public int minPos() {
        return 0;
    }

    @Override
    public int[] sort() {
        return new int[0];
    }

    @Override
    public int get(int pos) {
        return 0;
    }

    @Override
    public int[] halfRevers() {
        return new int[0];
    }

    @Override
    public int[] revers() {
        return new int[0];
    }

    @Override
    public void set(int pos, int val) {

    }
}
