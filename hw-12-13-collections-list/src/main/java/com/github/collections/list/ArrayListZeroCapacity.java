package com.github.collections.list;

public class ArrayListZeroCapacity implements IList{

    private int[] array = new int[0];

    @Override
    public void init(int[] init) {
        if (init == null) {
            this.array = new int[0];
        }else {
            this.array = new int[init.length];
            for (int i = 0; i < init.length; i++) {
                this.array[i] = init[i];
            }
        }

    }

    @Override
    public void clear() {
        this.array = new int[0];
    }

    @Override
    public int size() {
        return this.array.length;
    }

    @Override
    public int[] toArray() {
        int size = size();
        int[] result = new int[size];
        for (int i = 0; i < size; i++) {
            result[i] = this.array[i];
        }
        return result;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < size(); i++) {
            str.append(this.array[i]).append(", ");
        }
        return "";
    }

    @Override
    public void addStart(int val) {
        int size = size();
        int[] tmp = new int[size + 1];
        tmp[0] = val;
        for (int i = 0; i < size; i++) {
            tmp[i + 1] = this.array[i];
        }
        this.array = tmp;
    }

    @Override
    public void addEnd(int val) {

    }

    @Override
    public void addByPos(int pos, int val) {

    }

    @Override
    public void removeStart() {

    }

    @Override
    public void removeEnd() {

    }

    @Override
    public void removeByPos(int pos) {

    }

    @Override
    public int max() {
        return 0;
    }

    @Override
    public int min() {
        return 0;
    }

    @Override
    public int maxPos() {
        return 0;
    }

    @Override
    public int minPos() {
        return 0;
    }

    @Override
    public int[] sort() {
        return new int[0];
    }

    @Override
    public int get(int pos) {
        return 0;
    }

    @Override
    public int[] halfRevers() {
        return new int[0];
    }

    @Override
    public int[] revers() {
        return new int[0];
    }

    @Override
    public void set(int pos, int val) {

    }
}
