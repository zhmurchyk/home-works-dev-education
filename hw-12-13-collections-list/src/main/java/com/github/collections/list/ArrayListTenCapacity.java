package com.github.collections.list;

public class ArrayListTenCapacity implements IList {

    private int[] array = new int[10];

    private int index;

    @Override
    public void init(int[] init) {
        if (init == null) {
            this.index = 0;
        } else {
            this.index = init.length;
            for (int i = 0; i < this.index; i++) {
                this.array[i] = init[i];
            }
        }
    }

    @Override
    public void clear() {
        this.index = 0;
    }

    @Override
    public int size() {
        return this.index;
    }

    @Override
    public int[] toArray() {
        int[] result = new int[this.index];
        for (int i = 0; i < this.index; i++) {
            result[i] = this.array[i];
        }
        return result;
    }

    @Override
    public String toString() {
        return "";
    }

    @Override
    public void addStart(int val) {
        for (int i = this.index; i > -1 ; i--) {
            this.array[i+1] = this.array[i];
        }
        this.array[0] = val;
        this.index++;
    }

    @Override
    public void addEnd(int val) {
        this.array[index] = val;
        this.index++;

    }

    @Override
    public void addByPos(int pos, int val) {
        for (int i = this.index; i > pos - 1 ; i--) {
            this.array[i+1] = this.array[i];
        }
        this.array[pos] = val;
        this.index++;

    }

    @Override
    public void removeStart() {
        for (int i = 0; i < this.index-1; i++) {
            this.array[i] = this.array[i+1];
        }
        this.index--;
    }

    @Override
    public void removeEnd() {
        this.index--;
    }

    @Override
    public void removeByPos(int pos) {
        for (int i = pos; i < this.index-1; i++) {
            this.array[i] = this.array[i+1];
        }
        this.index--;
    }

    @Override
    public int max() {
        int max = this.array[0];
        for (int i =1; i < this.index; i++) {
            if (max < this.array[i]) {
                max = this.array[i];
            }
        }
        return max;
    }

    @Override
    public int min() {
        int min = this.array[0];
        for (int i =1; i < this.index; i++) {
            if (min > this.array[i]) {
                min = this.array[i];
            }
        }
        return min;
    }

    @Override
    public int maxPos() {
        int max = this.array[0];
        int pos = 0;
        for (int i =1; i < this.index; i++) {
            if (max < this.array[i]) {
                max = this.array[i];
                pos = i;
            }
        }
        return pos;
    }

    @Override
    public int minPos() {
        int min = this.array[0];
        int pos = 0;
        for (int i =1; i < this.index; i++) {
            if (min > this.array[i]) {
                min = this.array[i];
                pos = i;
            }
        }
        return pos;
    }

    @Override
    public int[] sort() {
        return new int[0];
    }

    @Override
    public int get(int pos) {
        return 0;
    }

    @Override
    public int[] halfRevers() {
        return new int[0];
    }

    @Override
    public int[] revers() {
        return new int[0];
    }

    @Override
    public void set(int pos, int val) {

    }

}

