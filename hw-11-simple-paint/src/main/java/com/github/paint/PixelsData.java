package com.github.paint;

import java.awt.*;
import java.awt.image.BufferedImage;

public class PixelsData {

    public int[][] getPixelsFromImage(BufferedImage image, int width, int height) {
        int[][] pixelDataInfo = new int[width][height];
        for(int a = 0; a < width; a++) {
            for(int b = 0; b < height; b++) {
                int pixelData = image.getRGB(a, b);
                Color color = new Color(pixelData, true);
                int red = color.getRed();
                int green = color.getGreen();
                int blue = color.getBlue();
                int argb = 255 << 24 + red << 16 + green << 8 + blue;
                pixelDataInfo[a][b] = argb;
            }
        }
        return pixelDataInfo;
    }
}
