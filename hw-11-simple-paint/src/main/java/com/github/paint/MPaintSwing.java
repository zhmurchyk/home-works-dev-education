package com.github.paint;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

public class MPaintSwing extends JPanel {

    private BufferedImage image;

    private Graphics2D graphics2D;

    public Color color = Color.BLACK;

    private double currentX, currentY, oldX, oldY;

    private int width = 700;

    private int height = 700;

    private int[][] pixels = new int[width][height];

    private ImageData getData;

    private PixelsData pixelsData;

    public MPaintSwing() {

        setDoubleBuffered(false);
        setVisible(true);
        setBounds(5, 5, width, height);
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                oldX = e.getX();
                oldY = e.getY();
            }

            public void mouseReleased(MouseEvent e) {
                oldX = currentX;
                oldY = currentY;
            }
        });
        addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                currentX = e.getX();
                currentY = e.getY();
                if (graphics2D != null) {
                    graphics2D.draw(new Line2D.Double(oldX, oldY, currentX, currentY));
                    repaint();
                    oldX = currentX;
                    oldY = currentY;
                }
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        if (image == null) {
            image = (BufferedImage) createImage(getSize().width, getSize().height);
            graphics2D = (Graphics2D) image.getGraphics();
            graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            clear();
        }
        g.drawImage(image, 0, 0, null);
    }

    public void clear() {
        graphics2D.setPaint(Color.WHITE);
        graphics2D.fillRect(0, 0, getSize().width, getSize().height);
        graphics2D.setPaint(this.color);
        repaint();
    }

    public void colorSelect() {
        this.color = JColorChooser.showDialog(new JFrame(), "Choose color", Color.BLACK);
        graphics2D.setPaint(color);
    }

    public void setBrushSize(int brushSize) {
        graphics2D.setStroke(new BasicStroke(brushSize, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
    }

    public void saveImg(String path) {
        for (int a = 0; a < width; a++) {
            for (int b = 0; b < height; b++) {
                int pixelData = this.image.getRGB(a, b);
                Color color = new Color(pixelData, true);
                int argb = color.getRGB();
                this.pixels[a][b] = argb;
            }
        }
        ImageData data = new ImageData(this.width, this.height, this.pixels);
        try {
            File file = new File(path);
            if (!file.exists()) {
                file.createNewFile();
            }
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(file, data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void openImg(String path) {
        try {
            File file = new File(path);
            ObjectMapper mapper = new ObjectMapper();
            ImageData data = mapper.readValue(file, ImageData.class);
            this.pixels = data.pixelsData;
            this.width = data.widthData;
            this.height = data.heightData;
            for (int a = 0; a < width; a++) {
                for (int b = 0; b < height; b++) {
                    this.image.setRGB(a, b, pixels[a][b]);
                }
            }
            repaint();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getColor() {
        Color colorData = this.color;
        return "[red = " + colorData.getRed() +
                ", green = " + colorData.getGreen() +
                ", blue = " + colorData.getBlue() +
                " ]";
    }

    @Override
    public String toString() {
        return "MPaintSwing {" +
                "brushColor = " + getColor() +
                ", canvasWidth = " + width +
                ", canvasHeight = " + height +
                ", pixelsData = " + Arrays.deepToString(pixels) +
                '}';
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    public int[][] getPixels() {
        return pixels;
    }

    public static class ImageData {

        public int widthData = 0;

        public int heightData = 0;

        public int[][] pixelsData = null;

        public ImageData() {
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ImageData imageData = (ImageData) o;
            return widthData == imageData.widthData && heightData == imageData.heightData && Arrays.equals(pixelsData, imageData.pixelsData);
        }

        @Override
        public int hashCode() {
            int result = Objects.hash(widthData, heightData);
            result = 31 * result + Arrays.hashCode(pixelsData);
            return result;
        }

        public ImageData(int width, int height, int[][] pixData) {
            this.pixelsData = pixData;
            this.heightData = height;
            this.widthData = width;
        }
    }
}
