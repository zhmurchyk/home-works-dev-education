package com.github.paint;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PaintFrame extends JPanel {

    private JButton colorSelect, clearButton, saveButton, loadButton;

    private JSlider setBrushSize;

    private JTextField valueForBrushSize;

    private MPaintSwing MPaintSwing;

    private JTable table;

    JFileChooser fileChooser = new JFileChooser();

    ActionListener actionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == colorSelect) {
                MPaintSwing.colorSelect();
                table.setBackground(MPaintSwing.color);
            } else if (e.getSource() == clearButton) {
                MPaintSwing.clear();
            } else if (e.getSource() == saveButton) {
                int returnValue = fileChooser.showSaveDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    String path = fileChooser.getSelectedFile().getAbsolutePath();
                    MPaintSwing.saveImg(path);
                }
            } else if (e.getSource() == loadButton) {
                int returnValue = fileChooser.showOpenDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    String path = fileChooser.getSelectedFile().getAbsolutePath();
                    MPaintSwing.openImg(path);
                }
            }
        }
    };

    ChangeListener changeListener = new ChangeListener() {
        @Override
        public void stateChanged(ChangeEvent e) {
            if (e.getSource() == setBrushSize) {
                valueForBrushSize.setText(String.valueOf(setBrushSize.getValue()));
                MPaintSwing.setBrushSize(Integer.parseInt(valueForBrushSize.getText()));
            }
        }
    };

    public void DrawFrame() {
        JLabel brushSizeLabel, labelInfoFile, brushSettings;
        JFrame frame = new JFrame("Simple Paint");
        JPanel panel = new JPanel();
        panel.setVisible(true);
        panel.setLayout(null);
        panel.setBounds(710, 5, 200, 160);
        panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        JPanel filePanel = new JPanel();
        filePanel.setVisible(true);
        filePanel.setLayout(null);
        filePanel.setBounds(710, 170, 200, 70);
        filePanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        MPaintSwing = new MPaintSwing();

        brushSettings = new JLabel("Paint tools");
        brushSettings.setBounds(5,10, 100,10);

        brushSizeLabel = new JLabel("Set brush size");
        brushSizeLabel.setBounds(7, 110, 100, 10);

        valueForBrushSize = new JTextField();
        valueForBrushSize.setBounds(120, 128, 25, 20);

        colorSelect = new JButton("Select Color");
        colorSelect.setBounds(5, 25, 110, 30);
        colorSelect.addActionListener(actionListener);

        table = new JTable(1, 1);
        table.setBounds(120, 25, 30, 30);
        table.setModel(new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });
        table.setBackground(MPaintSwing.color);
        table.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        clearButton = new JButton("Clear canvas");
        clearButton.setBounds(5, 65, 110, 30);
        clearButton.addActionListener(actionListener);

        setBrushSize = new JSlider(1, 100);
        setBrushSize.setBounds(5, 125, 110, 30);
        setBrushSize.addChangeListener(changeListener);

        labelInfoFile = new JLabel("File options");
        labelInfoFile.setBounds(5, 5, 100, 20);

        saveButton = new JButton("Save");
        saveButton.setBounds(5, 30, 70, 30);
        saveButton.addActionListener(actionListener);

        loadButton = new JButton("Load");
        loadButton.setBounds(80, 30, 70, 30);
        loadButton.addActionListener(actionListener);

        panel.add(brushSettings);
        panel.add(colorSelect);
        panel.add(table);
        panel.add(clearButton);
        panel.add(setBrushSize);
        panel.add(valueForBrushSize);
        panel.add(brushSizeLabel);

        filePanel.add(labelInfoFile);
        filePanel.add(saveButton);
        filePanel.add(loadButton);

        frame.setLayout(null);
        frame.setBounds(10, 10, 930, 750);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        frame.add(MPaintSwing);
        frame.add(panel);
        frame.add(filePanel);
    }
}
