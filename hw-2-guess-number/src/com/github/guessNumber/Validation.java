package com.github.guessNumber;

public class Validation {

    static int min, max, attempts;

    public static void validation() {

        System.out.print("Enter number min of range: ");
        min = ScannerClass.getNumber();

        System.out.print("Enter number max of range: ");
        max = ScannerClass.getNumber();

        System.out.print("Enter number of attempts: ");
        attempts = ScannerClass.getNumber();


        if (min < 1 || max < 1 || max > 200 || min >= max || attempts < 1 || attempts > 15)
            System.out.print("Wrong input ");
    }
}

