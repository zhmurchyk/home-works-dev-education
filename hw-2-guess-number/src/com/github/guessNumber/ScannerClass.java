package com.github.guessNumber;

import java.util.Scanner;

public class ScannerClass {

    private static final Scanner SCANNER = new Scanner(System.in);

    public static int getNumber() {
        return SCANNER.nextInt();
    }

    public static String getCommand() {
        return SCANNER.next();
    }
}
