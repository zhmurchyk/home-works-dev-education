package com.github.guessNumber;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner num = new Scanner(System.in);
        int min, max, guess = 0, attempts;
        System.out.print("Enter number min of range: ");
        min = num.nextInt();
        System.out.print("Enter number max of range: ");
        max = num.nextInt();
        System.out.print("Enter number of attempts: ");
        attempts = num.nextInt();
        if (min < 1 || max < 1 || max > 200 || min >= max || attempts < 1 || attempts > 15)
            System.out.print("Wrong input ");
        else guess = rnd(min, max);
        System.out.println(guess);
        System.out.print("Try to guess number from " + min + " to " + max + " by " + attempts + " attempts. Good Luck!\nEnter number: ");
        int number = num.nextInt();
        for (int i = attempts-1; true ; i--) {
            if ( number == guess ) {
                System.out.print("Congratulations!");
                break;
            }
            else if ( number > guess && i != 0) {
                System.out.print("Number is too big. try again. You have " + i + " attempts.\nEnter number: ");
                number = num.nextInt();
            }
            else if ( number < guess && i != 0) {
                System.out.print("Number is too small. try again. You have " + i + " attempts.\nEnter number: ");
                number = num.nextInt();
            }
            else {
                System.out.print("You lose");
                break;
            }
        }


    }

    public static int rnd(int min, int max)
    {
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

}
