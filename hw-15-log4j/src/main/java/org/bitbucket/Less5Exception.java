package org.bitbucket;

public class Less5Exception extends Exception {
    private int number;
    public int getNumber(){return number;}
    public Less5Exception(String message, int num){
        super(message);
        number=num;
    }
}
