package org.bitbucket;

import org.apache.log4j.Logger;

public class Log4j {

    private static final Logger log = Logger.getLogger(Log4j.class);

    public static void main(String[] args) {
        try {
            int result = getNumber();
            System.out.println(result);
            log.info("Run successful");

        } catch(Less5Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println(ex.getNumber());
            log.info("The number is less than 6");
        }

    }
    public static int getNumber () throws Less5Exception{
        int a = 0;
        int b = 10;
        int num = (int)(a + (Math.random() * b));
        if(num < 6) throw new Less5Exception("The number is less than 6", num);
        else return num;
    }
}
