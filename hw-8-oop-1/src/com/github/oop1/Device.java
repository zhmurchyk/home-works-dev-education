package com.github.oop1;

import java.util.ArrayList;

public class Device {

    Processor processor;
    Memory memory;

    public Device(Processor processor, Memory memory) {
        this.processor = processor;
        this.memory = memory;
    }

    public void save(String[] data) {
        for(String str : data){
            this.memory.save(str);
        }
    }

    public String[] readAll() {
        ArrayList<String> resultList = new ArrayList<>();
        while (true) {
            try {
                resultList.add(memory.readLast());
                memory.removeLast();
            } catch (NullPointerException e) {
                break;
            }
        }
        String[] resultArray = new String[resultList.size()];
        resultArray = resultList.toArray(resultArray);
        return resultArray;
    }

    void dataProcessing() {
        String[] readArray = this.memory.memoryCell;
        for(int i = 0; i < readArray.length; i++){
            readArray[i] = readArray[i].toLowerCase();
        }
    }

    public String getSystemInfo() {
        return "Device{" + "processor=" + processor.toString() +
                ", memory=" + memory.toString() + '}';
    }

    public Processor getProcessor() {
        return processor;
    }

    public Memory getMemory() {
        return memory;
    }

    public static Device[] getArms(Device[] devices){
        ArrayList<Device> resultList = new ArrayList<>();
        for(Device device : devices){
            if(device.getProcessor() instanceof ProcessorArm){
                resultList.add(device);
            }
        }
        Device[] resultArray = new Device[resultList.size()];
        return resultList.toArray(resultArray);
    }

    public static Device[] getX86(Device[] devices){
        ArrayList<Device> resultList = new ArrayList<>();
        for(Device device : devices){
            if(device.getProcessor() instanceof ProcessorX86){
                resultList.add(device);
            }
        }
        Device[] resultArray = new Device[resultList.size()];
        return resultList.toArray(resultArray);
    }

    public static Device[] byProcessorFrequency(String frequency, Device[] devices){
        ArrayList<Device> resultList = new ArrayList<>();
        for(Device device : devices){
            if(device.processor.frequency.equals(frequency)){
                resultList.add(device);
            }
        }
        Device[] resultArray = new Device[resultList.size()];
        return resultList.toArray(resultArray);
    }

    public static Device[] byProcessorCache(String cache, Device[] devices){
        ArrayList<Device> resultList = new ArrayList<>();
        for(Device device : devices){
            if(device.processor.cache.equals(cache)){
                resultList.add(device);
            }
        }
        Device[] resultArray = new Device[resultList.size()];
        return resultList.toArray(resultArray);
    }

    public static Device[] byProcessorBitCapacity(String bitCapacity, Device[] devices){
        ArrayList<Device> resultList = new ArrayList<>();
        for(Device device : devices){
            if(device.processor.bitCapacity.equals(bitCapacity)){
                resultList.add(device);
            }
        }
        Device[] resultArray = new Device[resultList.size()];
        return resultList.toArray(resultArray);
    }

    public static Device[] memoryMoreThan(int memorySize, Device[] devices){
        ArrayList<Device> resultList = new ArrayList<>();
        for(Device device : devices){
            if(device.getMemory().memorySize > memorySize){
                resultList.add(device);
            }
        }
        Device[] resultArray = new Device[resultList.size()];
        return resultList.toArray(resultArray);
    }

    public static Device[] memoryLessThan(int memorySize, Device[] devices){
        ArrayList<Device> resultList = new ArrayList<>();
        for(Device device : devices){
            if(device.getMemory().getMemoryInfo().countCells < memorySize){
                resultList.add(device);
            }
        }
        Device[] resultArray = new Device[resultList.size()];
        return resultList.toArray(resultArray);
    }

    public static Device[] memoryUsedMoreThan(float memoryUsed, Device[] devices){
        ArrayList<Device> resultList = new ArrayList<>();
        for(Device device : devices){
            if(device.getMemory().getMemoryInfo().memoryUsed > memoryUsed){
                resultList.add(device);
            }
        }
        Device[] resultArray = new Device[resultList.size()];
        return resultList.toArray(resultArray);
    }

    public static Device[] memoryUsedLessThan(float memoryUsed, Device[] devices){
        ArrayList<Device> resultList = new ArrayList<>();
        for(Device device : devices){
            if(device.getMemory().getMemoryInfo().memoryUsed < memoryUsed){
                resultList.add(device);
            }
        }
        Device[] resultArray = new Device[resultList.size()];
        return resultList.toArray(resultArray);
    }
}
