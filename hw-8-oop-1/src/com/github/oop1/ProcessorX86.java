package com.github.oop1;

public class ProcessorX86 extends Processor {

    final String ARCHITECTURE = "X86";

    public ProcessorX86() {}

    public ProcessorX86(String arch, String frequency, String cache, String bitCapacity) {
        this.frequency = frequency;
        this.cache = cache;
        this.bitCapacity = bitCapacity;
    }

    @Override
    public String dataProcess(String data) {
        return data.toLowerCase();
    }

    @Override
    public String dataProcess(long data) {
        return Long.toString(data);
    }
}
