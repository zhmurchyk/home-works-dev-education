package com.github.oop1;

import java.util.Arrays;

public class Memory {

    String[] memoryCell = null;

    public int memorySize;

    public Memory(String[] memoryCell) {
        this.memoryCell = memoryCell;
    }

    public Memory(int memorySize) {
        this.memorySize = memorySize;
        this.memoryCell = new String[memorySize];
    }

    public int getMemorySize() {
        return memorySize;
    }

    public String readLast() {
        for (int i = memoryCell.length - 1; i >= 0; i--) {
            if (memoryCell[i] != null) {
                return memoryCell[i];
            }
        }
        throw new NullPointerException("All cells are null.");
    }

    public String removeLast(){
        if(memoryCell.length == 0){
            throw new IllegalArgumentException("MemoryCell is empty.");
        }
        for(int i = memoryCell.length - 1; i >= 0; i--){
            if(memoryCell[i] != null){
                String tmp = memoryCell[i];
                memoryCell[i] = null;
                return tmp;
            }
        }
        throw new NullPointerException("All cells are null.");
    }

    public boolean save(String data){
        if(data == null){
            return false;
        }
        if(memoryCell.length == 0){
            throw new IllegalArgumentException("MemoryCell is empty.");
        }
        for(int i = memoryCell.length - 1; i >= 0; i--){
            if(memoryCell[i] == null){
                memoryCell[i] = data;
                return true;
            }
        }
        return false;
    }

    SettingsMemory getMemoryInfo() {
        if(memoryCell.length == 0){
            throw new IllegalArgumentException("MemoryCell is empty.");
        }
        float percentage = 0;
        float percentagePerCell = 100.f / memorySize;
        for (int i = 0; i < memorySize; i++){
            if(memoryCell[i] != null){
                percentage += percentagePerCell;
            }
        }
        return new SettingsMemory(memorySize, percentage);
    }

    @Override
    public String toString() {
        return "Memory{" +
                "memoryCell=" + Arrays.toString(memoryCell) +
                '}';
    }

}
