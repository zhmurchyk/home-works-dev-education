package com.github.oop1;

public abstract class Processor {

    public String frequency;
    public String cache;
    public String bitCapacity;

    public Processor(String frequency, String cache, String bitCapacity) {
        this.frequency = frequency;
        this.cache = cache;
        this.bitCapacity = bitCapacity;
    }

    public Processor(){}

    public String getDetails() {
        return frequency + ", " + cache + ", " + bitCapacity;
    }

    public abstract String dataProcess(String data);

    public abstract String dataProcess(long data);


}

