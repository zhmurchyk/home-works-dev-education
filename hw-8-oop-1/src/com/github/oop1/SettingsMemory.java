package com.github.oop1;

public class SettingsMemory {

    public int countCells;
    public float memoryUsed;

    public SettingsMemory(int numberOfCells, float memoryUsed) {
        this.countCells = numberOfCells;
        this.memoryUsed = memoryUsed;
    }

    public int getNumberOfCells() {
        return countCells;
    }

    public float getMemoryUsed() {
        return memoryUsed;
    }

    @Override
    public String toString() {
        return "Number of cells: " + countCells + ", Memory used: " + memoryUsed;
    }

}
