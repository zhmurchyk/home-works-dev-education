package com.github.oop1;

public class Dates {

    public static ProcessorArm processorArm_1 = new ProcessorArm("ARM","1.0", "1", "4");
    public static ProcessorArm processorArm_2 = new ProcessorArm("ARM","1.5", "2", "8");
    public static ProcessorArm processorArm_3 = new ProcessorArm("ARM","2.0", "2", "16");
    public static ProcessorX86 processorX86_1 = new ProcessorX86("X86","1.0", "1", "8");
    public static ProcessorX86 processorX86_2 = new ProcessorX86("X86","1.5", "2", "8");
    public static ProcessorX86 processorX86_3 = new ProcessorX86("X86","2.0", "4", "16");
    public static Memory memory1 = new Memory(new String[]{"1xx", null});
    public static Memory memory2 = new Memory(new String[]{"1xx", "2yy", null, null});
    public static Memory memory3 = new Memory(new String[]{"1xx", "2yy", "3zz", null, null, null});

    public static Device[] devices = new Device[10];

    static {
        devices[0] = new Device(processorX86_3, memory1);
        devices[1] = new Device(processorX86_2, memory2);
        devices[2] = new Device(processorX86_1, memory1);
        devices[3] = new Device(processorArm_1, memory3);
        devices[4] = new Device(processorArm_2, memory2);
        devices[5] = new Device(processorArm_3, memory1);
        devices[6] = new Device(processorArm_1, memory3);
        devices[7] = new Device(processorX86_2, memory3);
        devices[8] = new Device(processorArm_2, memory2);
        devices[9] = new Device(processorX86_3, memory3);
    }
}
