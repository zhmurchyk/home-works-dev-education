package com.github.oop1;

public class ProcessorArm extends Processor {

    final String ARCHITECTURE = "ARM";

    public ProcessorArm() {}

    public ProcessorArm(String arch, String frequency, String cache, String bitCapacity) {
        this.frequency = frequency;
        this.cache = cache;
        this.bitCapacity = bitCapacity;
    }

    @Override
    public String dataProcess(String data) {
        return data.toUpperCase();
    }

    @Override
    public String dataProcess(long data) {
        return Long.toString(data);
    }

    @Override
    public String toString() {
        return "ProcessorArm{" +
                "ARCHITECTURE=" + ARCHITECTURE +", frequency=" + frequency +
                ", cache=" + cache +
                ", bitCapacity=" + bitCapacity +
                '}';
    }
}
