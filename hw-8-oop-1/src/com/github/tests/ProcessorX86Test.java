package com.github.tests;

import com.github.oop1.Processor;
import com.github.oop1.ProcessorArm;
import com.github.oop1.ProcessorX86;
import org.junit.Assert;
import org.junit.Test;

public class ProcessorX86Test {

    private Processor processor;

    @Test
    public void dataProcess() {
        processor = new ProcessorX86();
        String exp = "test";
        String act = processor.dataProcess("TEST");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void testDataProcess() {
        processor = new ProcessorArm();
        String exp = "123123123";
        String act = processor.dataProcess("123123123");
        Assert.assertEquals(exp, act);
    }
}