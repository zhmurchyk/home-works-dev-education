package com.github.tests;

import com.github.oop1.*;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class DeviceTest {

    private static Memory memory1 = new Memory(new String[2]);
    private static Device device = new Device(new ProcessorArm("ARM", "1.0", "2", "4"), memory1);
    private static final String[] data = new String[2];

    @BeforeClass
    public static void setUp() {
        data[0] = "data0";
        data[1] = "data1";
        device.save(data);
    }

    @Test
    public void readAll() {
        String[] exp = data.clone();
        String[] act = device.readAll();
        Assert.assertArrayEquals(exp, act);

    }

    @Test
    public void getSystemInfo() {
        String exp = "Device{processor=ProcessorArm{ARCHITECTURE=ARM, frequency=1.0, cache=2, bitCapacity=4}, memory=Memory{memoryCell=[data1, data0]}}";
        String act = device.getSystemInfo();
        Assert.assertEquals(exp, act);

    }

    @Test
    public void getArms() {
        Device[] exp = new Device[5];
        exp[0] = Dates.devices[3];
        exp[1] = Dates.devices[4];
        exp[2] = Dates.devices[5];
        exp[3] = Dates.devices[6];
        exp[4] = Dates.devices[8];
        Device[] act = Device.getArms(Dates.devices);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void getX86() {
        Device[] exp = new Device[5];
        exp[0] = Dates.devices[0];
        exp[1] = Dates.devices[1];
        exp[2] = Dates.devices[2];
        exp[3] = Dates.devices[7];
        exp[4] = Dates.devices[9];
        Device[] act = Device.getX86(Dates.devices);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void byProcessorFrequency() {
        Device[] exp = new Device[3];
        exp[0] = Dates.devices[2];
        exp[1] = Dates.devices[3];
        exp[2] = Dates.devices[6];
        Device[] act = Device.byProcessorFrequency("1.0", Dates.devices);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void byProcessorCache() {
        Device[] exp = new Device[2];
        exp[0] = Dates.devices[0];
        exp[1] = Dates.devices[9];
        Device[] act = Device.byProcessorCache("4", Dates.devices);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void byProcessorBitCapacity() {
        Device[] exp = new Device[2];
        exp[0] = Dates.devices[3];
        exp[1] = Dates.devices[6];
        Device[] act = Device.byProcessorBitCapacity("4", Dates.devices);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void memoryMoreThan() {
        Device[] exp = new Device[7];
        exp[0] = Dates.devices[1];
        exp[1] = Dates.devices[3];
        exp[2] = Dates.devices[4];
        exp[3] = Dates.devices[6];
        exp[4] = Dates.devices[7];
        exp[5] = Dates.devices[8];
        exp[6] = Dates.devices[9];
        Device[] act = Device.memoryMoreThan(3, Dates.devices);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void memoryLessThan() {
        Device[] exp = new Device[3];
        exp[0] = Dates.devices[0];
        exp[1] = Dates.devices[2];
        exp[2] = Dates.devices[5];
        Device[] act = Device.memoryLessThan(3, Dates.devices);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void memoryUsedMoreThan() {
        Device[] exp = new Device[0];
        Device[] act = Device.memoryUsedMoreThan(60, Dates.devices);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void memoryUsedLessThan() {
        Device[] exp = Dates.devices;
        Device[] act = Device.memoryUsedLessThan(60, Dates.devices);
        Assert.assertArrayEquals(exp, act);
    }
}