package com.github.tests;

import com.github.oop1.Memory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MemoryTest {

    private Memory memory;

    @Before
    public void setUp(){
        memory = new Memory(new String[] {null, null});
    }

    @Test
    public void readLastOne(){
        memory.save("Test");
        Assert.assertEquals("Test", memory.readLast());
    }

    @Test
    public void removeLast() {
        memory.save("Test");
        Assert.assertEquals("Test", memory.removeLast());
    }

    @org.junit.Test
    public void save() {
        Assert.assertTrue(memory.save("Test"));
    }
}