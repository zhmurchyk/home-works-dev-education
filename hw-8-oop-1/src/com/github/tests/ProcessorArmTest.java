package com.github.tests;

import com.github.oop1.Processor;
import com.github.oop1.ProcessorArm;
import org.junit.Assert;
import org.junit.Test;

public class ProcessorArmTest {

    private Processor processor;

    @Test
    public void dataProcess() {
        processor = new ProcessorArm();
        String exp = "TEST";
        String act = processor.dataProcess("test");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void testDataProcess() {
        processor = new ProcessorArm();
        String exp = "123123123";
        String act = processor.dataProcess("123123123");
        Assert.assertEquals(exp, act);
    }
}