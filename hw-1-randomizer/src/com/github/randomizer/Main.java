package com.github.randomizer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner num = new Scanner(System.in);
        int min, max;
        System.out.print("Enter number min of range: ");
        min = num.nextInt();
        System.out.print("Enter number max of range: ");
        max = num.nextInt();
        if (min < 1 || min > 500 || max < 1 || max > 500 || min >= max)
            System.out.print("Wrong input ");
        else {
            ArrayList<Integer> list = new ArrayList<Integer>();
            for (int i = min; i < max + 1; i++) {
                list.add(new Integer(i));
                Collections.shuffle(list);

            }
            generate(list);
        }
    }
    public static void generate(ArrayList<Integer> list) {
        Scanner com = new Scanner(System.in);
        System.out.print("Enter command (“generate”, “exit”, “help”) ");
        String command = com.nextLine();
        if (command.equals("help")) {
            System.out.print("Instruction");
            System.exit(0);
        }
        else if (command.equals("exit")) {
            System.out.print("Program will be closed");
            System.exit(0);;
        }
        else if (command.equals("generate") && list.size() != 0) {
            for (int i = 0; i < list.size(); i++) {
                System.out.println(list.get(i));
                list.remove(i);
                generate(list);

            }
        }
        else System.out.print("All numbers are generated");
    }
}
